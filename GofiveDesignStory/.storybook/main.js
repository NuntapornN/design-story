module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@ergosign/storybook-addon-pseudo-states-angular/preset-postcss",
    "@geometricpanda/storybook-addon-badges",
    "@storybook/addon-a11y",
    "@storybook/addon-essentials",
    "@storybook/addon-links",
    "storybook-addon-live-examples",
    "storybook-addon-mock/register",
    // "storybook-addon-playroom",
    "storybook-addon-paddings",
    "storybook-addon-performance/register",
    "storybook-formik/register"
  ],
  "core": {
    "builder": "webpack5"
  }
}